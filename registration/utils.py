from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site

from models import RegistrationProfile


def resend_activation_emails(users_ids):
    '''
        resend activation email to given user ids
    '''
    User = get_user_model()
                        
    if Site._meta.installed:
        site = Site.objects.get_current()
    
    registration_profiles = RegistrationProfile.objects.filter(user__id__in=users_ids)
    for rp in registration_profiles:
        if rp.activation_key_expired():
            rp.update_activation_key()
        rp.send_activation_email(site)
    
    users_no_registration_profiles = User.objects.filter(id__in=users_ids, registrationprofile__isnull=True)    
    for u in users_no_registration_profiles:
        rp = RegistrationProfile.objects.create_profile(u)
        rp.send_activation_email(site)
        