from django.conf import settings

def get_email_backend():
    if hasattr(settings, 'REGISTRATION_EMAIL_BACKEND'):
        backend = getattr(settings, 'REGISTRATION_EMAIL_BACKEND')
    else:
        backend = getattr(settings, 'EMAIL_BACKEND',
                          'django.core.mail.backends.smtp.EmailBackend')
    return backend
